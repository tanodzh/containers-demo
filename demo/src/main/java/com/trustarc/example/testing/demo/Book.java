package com.trustarc.example.testing.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Book {
	@Id
	private String id;

	@Column(nullable = false)
	private String title;

	@Column(nullable = false)
	private Integer size;

	@ManyToOne(optional = false)
	private Person author;

	@ManyToOne(optional = false)
	private Person producer;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Person getAuthor() {
		return author;
	}

	public void setAuthor(Person author) {
		this.author = author;
	}

	public Person getProducer() {
		return producer;
	}

	public void setProducer(Person producer) {
		this.producer = producer;
	}
}
