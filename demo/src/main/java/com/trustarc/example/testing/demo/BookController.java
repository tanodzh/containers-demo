package com.trustarc.example.testing.demo;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("book")
public class BookController {

	private final LibraryService libraryService;

	public BookController(LibraryService libraryService) {
		super();
		this.libraryService = libraryService;
	}

	@PostMapping
	public Book create(@Valid @RequestBody Book book) {
		return libraryService.save(book);
	}

	@GetMapping
	public Collection<Book> getAllBooks() {
		return libraryService.findAll();
	}
}
