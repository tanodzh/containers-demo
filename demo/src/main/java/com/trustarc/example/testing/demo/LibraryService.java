package com.trustarc.example.testing.demo;

import java.util.Collection;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.stereotype.Service;

@Service
public class LibraryService {

	private final BookRepository bookRepository;
	private final PersonRepository personRepository;

	public LibraryService(BookRepository bookRepository, PersonRepository personRepository) {
		super();
		this.bookRepository = bookRepository;
		this.personRepository = personRepository;
	}

	public Book save(Book book) {
		if (book.getId() == null) {
			book.setId(UUID.randomUUID().toString());
		}
		return bookRepository.save(book);
	}

	public Collection<Book> findAll() {
		return bookRepository.findAll();
	}

	public Person save(@Valid Person author) {
		if (author.getId() == null) {
			author.setId(UUID.randomUUID().toString());
		}
		return personRepository.save(author);
	}

	public Collection<Person> findAllPrsons() {
		return personRepository.findAllByOrderByFirstName();
	}
}
