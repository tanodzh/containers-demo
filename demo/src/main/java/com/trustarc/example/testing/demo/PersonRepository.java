package com.trustarc.example.testing.demo;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, String> {
	Collection<Person> findAllByOrderByFirstName();
}
