package com.trustarc.example.testing.demo;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("person")
public class PersonController {
	private final LibraryService libraryService;

	public PersonController(LibraryService libraryService) {
		super();
		this.libraryService = libraryService;
	}

	@PostMapping
	public Person createAuthor(@Valid @RequestBody Person author) {
		return libraryService.save(author);
	}

	@GetMapping
	public Collection<Person> getAllPersons() {
		return libraryService.findAllPrsons();
	}
}
