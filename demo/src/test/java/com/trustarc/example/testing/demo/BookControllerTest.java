package com.trustarc.example.testing.demo;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

@AutoConfigureMockMvc
public class BookControllerTest extends BaseTestH2 {
	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private LibraryService libraryService;

	private static final Person author = new Person();

	private static final Person producer = new Person();

	@BeforeClass
	public static void init() {
		author.setFirstName("William");
		author.setLastName("Shakespeare");
		author.setNationality("EN");
		author.setSex("MALE");

		producer.setFirstName("Robert");
		producer.setLastName("Jackson");
		producer.setNationality("EN");
		producer.setSex("MALE");
	}

	@Test
	public void testa() throws Exception {

		libraryService.save(author);
		libraryService.save(producer);

		final Book book = new Book();
		book.setTitle("Much Ado About Nothing");
		book.setAuthor(author);
		book.setProducer(producer);
		book.setSize(246);

		final ObjectWriter writer = new ObjectMapper().writerFor(Book.class);
		mockMvc.perform(
				post("/book").content(writer.writeValueAsString(book)).contentType(MediaType.APPLICATION_JSON_UTF8))
				.andDo(print()).andExpect(status().isOk())
				.andExpect(jsonPath("$.author.firstName", is(author.getFirstName())))
				.andExpect(jsonPath("$.author.lastName", is(author.getLastName())))
				.andExpect(jsonPath("$.producer.firstName", is(producer.getFirstName())))
				.andExpect(jsonPath("$.producer.lastName", is(producer.getLastName())));
	}

	@Test
	public void testb() throws Exception {
		mockMvc.perform(get("/book")).andDo(print()).andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(1)))
			.andExpect(jsonPath("$.[0].author.firstName", is(author.getFirstName())))
			.andExpect(jsonPath("$.[0].author.lastName", is(author.getLastName())))
			.andExpect(jsonPath("$.[0].producer.firstName", is(producer.getFirstName())))
			.andExpect(jsonPath("$.[0].producer.lastName", is(producer.getLastName())));
	}
	
	@Test(expected = DataIntegrityViolationException.class)
	public void testc() throws Throwable {
		final Book book = new Book();
		book.setTitle("Much Ado About Nothing");
		book.setAuthor(author);
		book.setProducer(producer);

		final ObjectWriter writer = new ObjectMapper().writerFor(Book.class);
		try {
			mockMvc.perform(
					post("/book").content(writer.writeValueAsString(book)).contentType(MediaType.APPLICATION_JSON_UTF8))
					.andDo(print()).andExpect(status().isOk());
		} catch (NestedServletException e) {
			throw e.getCause();
		}
	}
}
