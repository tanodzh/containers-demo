package com.trustarc.example.testing.demo;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
public class APersonControllerTest extends BaseTestH2 {
	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testa() throws Exception {
		mockMvc.perform(get("/person"))
			.andDo(print()).andExpect(status().isOk())
			.andExpect(jsonPath("$", hasSize(0)));
	}
}
