package com.trustarc.example.testing.demo;

import org.junit.ClassRule;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.PostgreSQLContainer;

@DirtiesContext
@SpringBootTest
@RunWith(SpringRunner.class)
@ContextConfiguration(initializers = { BaseTest.Initializer.class })
public abstract class BaseTest { // NOSONAR
	
	@ClassRule
	public static PostgreSQLContainer<?> postgreSQLContainer = new PostgreSQLContainer<>("postgres:10.9")
			.withDatabaseName("demo").withUsername("demo").withPassword("demo");

	static ConfigurableApplicationContext context;
	
	static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
		public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
			context = configurableApplicationContext;
			TestPropertyValues
					.of("spring.datasource.url=" + postgreSQLContainer.getJdbcUrl(),
							"spring.datasource.username=" + postgreSQLContainer.getUsername(),
							"spring.datasource.password=" + postgreSQLContainer.getPassword(),
							"spring.jpa.properties.hibernate.connection.release_mode=after_transaction")
					.applyTo(configurableApplicationContext.getEnvironment());
		}
	}
}
