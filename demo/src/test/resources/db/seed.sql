-- Drop table

-- DROP TABLE public.person;

CREATE TABLE public.person (
	id varchar(255) NOT NULL,
	first_name varchar(255) NOT NULL,
	last_name varchar(255) NOT NULL,
	nationality varchar(255) NOT NULL,
	sex varchar(255) NOT NULL,
	CONSTRAINT person_pkey PRIMARY KEY (id)
);

-- Drop table

-- DROP TABLE public.book;

CREATE TABLE public.book (
	id varchar(255) NOT NULL,
	author_id varchar(255) NOT NULL,
	producer_id varchar(255) NOT NULL,
	"size" int4 NOT NULL,
	title varchar(255) NOT NULL,
	CONSTRAINT book_pkey PRIMARY KEY (id)
);

ALTER TABLE public.book ADD CONSTRAINT fk8wjogaten58g6212m5vij8051 FOREIGN KEY (producer_id) REFERENCES person(id);
ALTER TABLE public.book ADD CONSTRAINT fki7lkcmacourlqkkn4uo1s4svl FOREIGN KEY (author_id) REFERENCES person(id);

INSERT INTO public.person
(id, first_name, last_name, nationality, sex)
VALUES('abe2b81b-70b7-4cbb-96c6-17300f196752', 'Tano', 'Dzhinski', 'BG', 'MALE');
INSERT INTO public.person
(id, first_name, last_name, nationality, sex)
VALUES('8716687b-8db4-471b-8c61-c90cfa507f7b', 'Clifford', 'Simak', 'USA', 'MALE');
